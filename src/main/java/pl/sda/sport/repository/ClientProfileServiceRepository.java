package pl.sda.sport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.Profile;



public interface ClientProfileServiceRepository extends JpaRepository<Profile, Long> {
    Profile findByOwner (Client client);
}
