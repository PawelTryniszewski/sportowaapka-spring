package pl.sda.sport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.sport.model.ClientRole;

import java.util.Optional;
@Repository
public interface ClientRoleRepository extends JpaRepository<ClientRole, Long> {
    Optional<ClientRole> findByName(String username);
}
