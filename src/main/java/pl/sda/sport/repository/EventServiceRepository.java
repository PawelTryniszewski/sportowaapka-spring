package pl.sda.sport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.Event;

import java.time.LocalDate;
import java.util.List;

public interface EventServiceRepository extends JpaRepository<Event, Long> {
    List<Event> findEventsByPlayerListContainingAndEventDateTimeIsGreaterThanEqualOrderByEventDateTimeAsc(Client client, LocalDate localDate);
    List<Event> findEventsByPlayerListContainingAndEventDateTimeIsLessThanOrderByEventDateTimeDesc(Client client, LocalDate localDate);
    List<Event> findAllByEventDateTimeIsGreaterThanEqualOrderByEventDateTimeAsc(LocalDate localDate);


}
