package pl.sda.sport.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.sport.model.AppilcationEvent;

@Repository
public interface ApplicationEventRepository extends JpaRepository<AppilcationEvent, Long> {
}
