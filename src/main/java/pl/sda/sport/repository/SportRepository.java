package pl.sda.sport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.sport.model.Sport;

import java.util.Optional;

public interface SportRepository extends JpaRepository<Sport, Long> {
    Optional<Sport> findByName(String name);

}
