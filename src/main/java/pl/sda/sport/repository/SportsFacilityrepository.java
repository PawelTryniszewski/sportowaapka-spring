package pl.sda.sport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.sport.model.SportsFacility;

@Repository
public interface SportsFacilityrepository extends JpaRepository<SportsFacility, Long> {

}
