package pl.sda.sport.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.sport.model.Event;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SportCreateDto {

    private String name;

    private List<Event> eventList;
}
