package pl.sda.sport.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.Sport;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventCreateDto {

    private Long id;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate eventDateTime;
    private Integer eventLengthInMinutes;
    private Integer entryFee;
    private Integer maxPlayers;
    private Client organizer;
    private Sport sport;
}
