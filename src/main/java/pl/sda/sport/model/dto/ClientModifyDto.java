package pl.sda.sport.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientModifyDto {


    private String username;

    @JsonIgnore
    @Size(min = 6, max = 255)
    private String password;

    @JsonIgnore
    @Size(min = 6, max = 255)
    private String passwordConfirm;

    @Email
    private String email;
}
