package pl.sda.sport.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Lob;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfilModifyDto {
    private Long id;
    private String name;
    private String surname;
    private Long phoneNumber;
    @Lob
    @Column
    private byte[] image;
}
