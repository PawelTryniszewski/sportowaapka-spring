package pl.sda.sport.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
//    private SportsFacility sportsFacility;

    private LocalDate eventDateTime;
    private Integer eventLengthInMinutes;
    private Integer entryFee;
    private Integer maxPlayers;

    @ManyToMany
    private List<Client> playerList;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Client organizer;


    @ManyToOne
    private Sport sport;
}
