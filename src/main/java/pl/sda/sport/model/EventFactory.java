package pl.sda.sport.model;

public final class EventFactory {
    private EventFactory() {
    }

    public static AppilcationEvent loginSuccess(String username, String address) {
        return AppilcationEvent.builder().event("Login success. IP: " + address).msg("User: " + username).build();
    }

    public static AppilcationEvent loginFailed(String username, String password, String address) {
        return AppilcationEvent.builder().event("Login failed. IP: " + address).msg("User: " + username + " used password: " + password).build();
    }

    public static AppilcationEvent supplierEventRemoveFailed(String message, String user) {
        return AppilcationEvent.builder().event(message).msg("User: " + user).build();
    }
}