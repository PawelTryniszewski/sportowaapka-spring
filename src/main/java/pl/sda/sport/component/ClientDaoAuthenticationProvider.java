package pl.sda.sport.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import pl.sda.sport.model.EventFactory;
import pl.sda.sport.repository.ApplicationEventRepository;

import javax.servlet.http.HttpServletRequest;

public class ClientDaoAuthenticationProvider extends DaoAuthenticationProvider {
    @Autowired
    private ApplicationEventRepository applicationEventRepository;

    @Autowired
    private HttpServletRequest request;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        AuthenticationException caughtException = null;
        Authentication authenticationResult = null;
        
        try {
            authenticationResult = super.authenticate(authentication);
        } catch (AuthenticationException authenticationException) {
            caughtException = authenticationException;
        }

        if (caughtException != null) {
            logAuthenticationAttempt(authentication.getName(), authentication.getCredentials().toString());
            throw caughtException;
        }

        if(authenticationResult == null){
            logAuthenticationAttempt(authentication.getName(), authentication.getCredentials().toString());
            return null;
        }
        return authenticationResult;
    }

    private void logAuthenticationAttempt(String name, String password) {
        applicationEventRepository.saveAndFlush(EventFactory.loginFailed(name, password, request.getRemoteAddr()));
    }
}
