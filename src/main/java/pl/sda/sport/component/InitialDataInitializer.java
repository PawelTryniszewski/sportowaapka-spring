package pl.sda.sport.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.ClientRole;
import pl.sda.sport.model.Profile;
import pl.sda.sport.repository.ClientRepository;
import pl.sda.sport.repository.ClientRoleRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Component
public class InitialDataInitializer implements
        ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private ClientRoleRepository clientRoleRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        addRole("ROLE_ADMIN");
        addRole("ROLE_USER");


        addClient("admin", "admin", "ROLE_ADMIN", "ROLE_USER");
        addClient("user", "user", "ROLE_USER");
    }

    @Transactional
    public void addRole(String name) {
        Optional<ClientRole> clientRoleOptional = clientRoleRepository.findByName(name);
        if (!clientRoleOptional.isPresent()) {
            clientRoleRepository.save(new ClientRole(null, name));
        }
    }

    @Transactional
    public void addClient(String username, String password, String... roles) {
        Optional<Client> clientOptional = clientRepository.findByUsername(username);
        if (!clientOptional.isPresent()) {
            List<ClientRole> rolesList = new ArrayList<>();
            for (String role : roles) {
                Optional<ClientRole> clientRoleOptional = clientRoleRepository.findByName(role);
                clientRoleOptional.ifPresent(rolesList::add);
            }

            clientRepository.saveAndFlush(new Client(
                    null, // id
//                    null, // name
//                    null, // surname
                    username, // username
                    passwordEncoder.encode(password), // password
                    null, // email
                    new HashSet<ClientRole>(rolesList)
                    ,new Profile()
//                    ,new ArrayList<>()
            )); // roles
        }
    }
}
