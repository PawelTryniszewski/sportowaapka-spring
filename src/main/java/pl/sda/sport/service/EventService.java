package pl.sda.sport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.Event;
import pl.sda.sport.model.Sport;
import pl.sda.sport.model.dto.EventCreateDto;
import pl.sda.sport.repository.EventServiceRepository;
import pl.sda.sport.repository.SportRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EventService {

    @Autowired
    private EventServiceRepository eventServiceRepository;
    @Autowired
    private SportRepository sportRepository;
    @Autowired
    private LoginService loginService;

    public Optional<Event> addEvent(EventCreateDto dto) {
        Sport football = sportRepository.findByName("football").get();
        Event event = new Event(null, dto.getName(), dto.getEventDateTime(), dto.getEventLengthInMinutes(), dto.getEntryFee(),
                dto.getMaxPlayers(), new ArrayList<>(dto.getMaxPlayers()), new Client(), new Sport());
        event.setSport(football);
        event.setOrganizer(loginService.getLoggedInUser().get());
        football.getEventList().add(event);
        Event newEvent = eventServiceRepository.saveAndFlush(event);
        sportRepository.saveAndFlush(football);
        return Optional.of(newEvent);
    }

    public Object getAll() {
        return eventServiceRepository.findAll();
    }

    public Object getAllAfterDate(LocalDate localDate) {
        return eventServiceRepository.findAllByEventDateTimeIsGreaterThanEqualOrderByEventDateTimeAsc(localDate);
    }

    public Optional<Event> deleteEventById(Long id) {
        Optional<Event> optionalEvent = eventServiceRepository.findById(id);
        if (optionalEvent.isPresent()) {
            Event event = optionalEvent.get();

            event.setOrganizer(null);
            event.setSport(null);
            eventServiceRepository.delete(event);
        }
        return Optional.empty();
    }

    public Object getUserEvents(LocalDate localDate) {
        Client client = loginService.getLoggedInUser().get();
        return eventServiceRepository.findEventsByPlayerListContainingAndEventDateTimeIsGreaterThanEqualOrderByEventDateTimeAsc(client, localDate);
    }

    public Optional<Event> addBasketEvent(EventCreateDto dto) {
        Sport basketball = sportRepository.findByName("basketball").get();
        Event event = new Event(null, dto.getName(), dto.getEventDateTime(), dto.getEventLengthInMinutes(), dto.getEntryFee(),
                dto.getMaxPlayers(), new ArrayList<>(dto.getMaxPlayers()), new Client(), new Sport());
        event.setSport(basketball);
        event.setOrganizer(loginService.getLoggedInUser().get());
        basketball.getEventList().add(event);
        sportRepository.saveAndFlush(basketball);
        Event newEvent = eventServiceRepository.saveAndFlush(event);
        return Optional.of(newEvent);
    }

    public Optional<Event> joinToEvent(Long id) {
        Optional<Event> optionalEvent = eventServiceRepository.findById(id);
        Client client = loginService.getLoggedInUser().get();
        if (optionalEvent.isPresent()) {
            Event event = optionalEvent.get();
            if (event.getPlayerList().size() < event.getMaxPlayers()) {
                event.getPlayerList().add(client);
                eventServiceRepository.saveAndFlush(event);
            }
        }
        return Optional.empty();

    }

    public Optional<Event> removeMeFromList(Long id) {
        Optional<Event> optionalEvent = eventServiceRepository.findById(id);
        Client client = loginService.getLoggedInUser().get();
        if (optionalEvent.isPresent()) {
            Event event = optionalEvent.get();
            event.getPlayerList().remove(client);
            eventServiceRepository.saveAndFlush(event);
        }
        return Optional.empty();
    }


    public Object getAllPlayers(Long id) {
        Optional<Event> byId = eventServiceRepository.findById(id);
        Event event = byId.get();
        List<Client> players = event.getPlayerList();
        return players;
    }

    public Object getUserEventsHistory(LocalDate localDate) {
        Client client = loginService.getLoggedInUser().get();
        return eventServiceRepository.findEventsByPlayerListContainingAndEventDateTimeIsLessThanOrderByEventDateTimeDesc(client, localDate);
    }
}
