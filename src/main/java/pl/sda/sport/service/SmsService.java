package pl.sda.sport.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.stereotype.Service;

@Service
public class SmsService {
    public static final String ACCOUNT_SID = "AC2d0819f7cd508f8d89c5839e49eaa312ad";
    public static final String AUTH_TOKEN = "40412711682d4ead314a3bd040adc72dad";

    public String sendSms() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
                new PhoneNumber("+48 Numer Do Kogo"),
                new PhoneNumber("+48 Numer z Twilio"),
                "Witamy w serwisie")
                .create();

        return message.getSid();
    }
}
