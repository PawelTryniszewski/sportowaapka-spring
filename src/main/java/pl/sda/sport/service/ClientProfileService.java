package pl.sda.sport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.Profile;
import pl.sda.sport.model.dto.ProfilModifyDto;
import pl.sda.sport.repository.ClientProfileServiceRepository;

import java.util.Optional;

@Service
public class ClientProfileService {
    @Autowired
    private ClientProfileServiceRepository clientProfileServiceRepository;

    public Object getAll() {return clientProfileServiceRepository.findAll();
    }

    public Object findProfile(Client client) {
        return clientProfileServiceRepository.findByOwner(client);
    }

    public Optional<Profile> findProfileById(Long id) {
        return clientProfileServiceRepository.findById(id);
    }

    public Optional<Profile> edit(ProfilModifyDto dto) {
        Optional<Profile> optionalProfile = clientProfileServiceRepository.findById(dto.getId());
        Profile profile = optionalProfile.get();
        profile.setName(dto.getName());
        profile.setSurname(dto.getSurname());
        profile.setPhoneNumber(dto.getPhoneNumber());
//        profile.setImage(bytes);
        clientProfileServiceRepository.saveAndFlush(profile);
        return Optional.of(profile);
    }


}
