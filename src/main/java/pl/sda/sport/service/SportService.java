package pl.sda.sport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.sport.model.Sport;
import pl.sda.sport.model.dto.SportCreateDto;
import pl.sda.sport.repository.SportRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class SportService {
    @Autowired
    private SportRepository sportRepository;

    public Optional<Sport> addSport(SportCreateDto dto) {
        Sport sport = new Sport(null,dto.getName(),new ArrayList<>());
        Sport newSport = sportRepository.saveAndFlush(sport);
        return Optional.of(newSport);
    }

    public Object getAll() {
        return sportRepository.findAll();
    }

    public Optional<Sport> deleteSportById(Long id) {
        Optional<Sport> optionalSport = sportRepository.findById(id);
        if (optionalSport.isPresent()){
            Sport sport = optionalSport.get();
            sportRepository.delete(sport);
        }
        return Optional.empty();
    }


}
