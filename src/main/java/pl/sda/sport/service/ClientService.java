package pl.sda.sport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.Profile;
import pl.sda.sport.model.dto.ClientModifyDto;
import pl.sda.sport.repository.ClientRepository;
import pl.sda.sport.repository.ClientRoleRepository;

import java.util.*;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private ClientRoleRepository clientRoleRepository;

    public Optional<Client> findByUsername(String username) {
        return clientRepository.findByUsername(username);
    }

    public List<Client> getAll() {
        return clientRepository.findAll();
    }

    public Optional<Client> register(ClientModifyDto dto) {
        Optional<Client> optionalEmployee = clientRepository.findByUsername(dto.getUsername());
        if (optionalEmployee.isPresent()) {
            return Optional.empty();
        }

        Client newClient = new Client();
        Profile profile = new Profile();
        profile.setOwner(newClient);
//        newClient.setName(dto.getName());
        newClient.setEmail(dto.getEmail());
        newClient.setPassword(encoder.encode(dto.getPassword()));
        newClient.setUsername(dto.getUsername());
        newClient.setRoles(new HashSet<>(Arrays.asList(clientRoleRepository.findByName("ROLE_USER").get())));
        newClient.setProfile(profile);


        newClient = clientRepository.save(newClient);

        return Optional.of(newClient);
    }

    public Optional<Client> deleteClientById(Long id) {
        Optional<Client> optionalClient = clientRepository.findById(id);
        if (optionalClient.isPresent()) {
            Client client = optionalClient.get();
            clientRepository.delete(client);
        }
        return Optional.empty();
    }
}
