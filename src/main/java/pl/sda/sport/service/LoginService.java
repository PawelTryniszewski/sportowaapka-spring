package pl.sda.sport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.EventFactory;
import pl.sda.sport.repository.ApplicationEventRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class LoginService implements UserDetailsService {
    @Autowired
    private ApplicationEventRepository applicationEventRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private HttpServletRequest request;
    private boolean admin;



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Client> clientOptional = clientService.findByUsername(username);
        if (clientOptional.isPresent()) {
            Client client = clientOptional.get();
            applicationEventRepository.save(EventFactory.loginSuccess(username, request.getRemoteAddr()));
            return User
                    .withUsername(client.getUsername())
                    .password(client.getPassword())
                    .roles(extractRoles(client))
                    .build();

        }
// ZROBIC KLASE CLIENTDAOAUTHENTICATIONPROVIDER
        throw new UsernameNotFoundException("User not found by name: " + username);
    }

    private String[] extractRoles(Client client) {
        List<String> roles = client.getRoles().stream().map(role -> role.getName().replace("ROLE_", "")).collect(Collectors.toList());
        String[] rolesArray = new String[roles.size()];
        rolesArray = roles.toArray(rolesArray);

        return rolesArray;
    }

    public Optional<Client> getLoggedInUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            // nie jesteśmy zalogowani
            return Optional.empty();
        }

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return clientService.findByUsername(user.getUsername());
            // jesteśmy zalogowani, zwracamy user'a
        }

        return Optional.empty();
    }
}
