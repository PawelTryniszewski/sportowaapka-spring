package pl.sda.sport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.sport.service.EventService;
import pl.sda.sport.service.LoginService;

import java.time.LocalDate;

@Controller
@RequestMapping("/")
public class IndexController {
    @Autowired
    private EventService eventService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/")
    public String getIndex(Model model) {

        model.addAttribute("eventList", eventService.getAllAfterDate(LocalDate.now()));
        if(loginService.getLoggedInUser().isPresent()) {
            model.addAttribute("currentUser", loginService.getLoggedInUser().get());
        }
        return "index";
    }

    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }
}
