package pl.sda.sport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.sport.model.Profile;
import pl.sda.sport.model.dto.ProfilModifyDto;
import pl.sda.sport.service.ClientProfileService;

import java.util.Optional;

@Controller
@RequestMapping("/profile/")
public class CilentProfileController {
    @Autowired
    private ClientProfileService clientProfileService;


    @GetMapping("/edit/{prof_id}")
    public String getModifyForm(@PathVariable(name = "prof_id") Long id, Model model) {
        Optional<Profile> optionalProfile = clientProfileService.findProfileById(id);
        if (optionalProfile.isPresent()) {
            ProfilModifyDto dto = new ProfilModifyDto();
            Profile profile = optionalProfile.get();
            dto.setId(profile.getId());
            dto.setName(profile.getName());
            dto.setSurname(profile.getSurname());
            dto.setPhoneNumber(profile.getPhoneNumber());
//            dto.setImage(Base64.getEncoder().encode(profile.getImage()));
            model.addAttribute("profile", profile);
            return "profile/edit";
        }
        return "redirect:/login";
    }

    @PostMapping("/edit")
    public String edit(Model model, ProfilModifyDto dto) {
//        ,@RequestParam("image") MultipartFile file
//        String name = file.getName();
//        byte[] bytes = null;
//        try {
//            bytes = file.getBytes();
//            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name + "-uploaded")));
//            stream.write(bytes);
//            stream.close();
//        } catch (Exception e) {
//            System.out.println("File has not been added.");
//        }
        Optional<Profile> optionalProfile = clientProfileService.edit(dto);
        if (optionalProfile.isPresent()) {

            model.addAttribute("profile", dto);
            model.addAttribute("erro_message", "Modification error");
            return "redirect:/client/profile";
        }
        return "profile/edit";
    }

}
