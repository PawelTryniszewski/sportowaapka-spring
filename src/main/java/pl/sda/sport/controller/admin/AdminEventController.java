package pl.sda.sport.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.sport.model.Event;
import pl.sda.sport.model.dto.EventCreateDto;
import pl.sda.sport.service.EventService;

import java.util.Optional;
@Controller
@RequestMapping("/adminEvent/")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminEventController {
    @Autowired
    private EventService eventService;

    @GetMapping("/list")
    public String eventList(Model model) {

        model.addAttribute("eventList", eventService.getAll());
        return "admin/client/event_list";
    }

    @GetMapping("/add")
    public String getSportForm(Model model) {
        model.addAttribute("event", new EventCreateDto());

        model.addAttribute("added_sport", "add");
        return "admin/client/event_add";
    }
    @GetMapping("/addBasketball")
    public String getSportFormB(Model model) {
        model.addAttribute("event", new EventCreateDto());
        model.addAttribute("added_sport", "addBasketball");
        return "admin/client/event_add";
    }

    @PostMapping("/add")
    public String sendEmployeeForm(EventCreateDto dto, Model model) {
        Optional<Event> eventOptional = eventService.addEvent(dto);
        if(eventOptional.isPresent()){
            Event event = eventOptional.get();
//            mailingService.sendEmail(client,"YeeeY!" );
//            Logger.getLogger(getClass()).info(smsService.sendSms());

            return "redirect:/adminEvent/list";
        }

        return "redirect:/adminEvent/list";
    }

    @PostMapping("/addBasketball")
    public String sendForm(EventCreateDto dto, Model model) {
        Optional<Event> eventOptional = eventService.addBasketEvent(dto);
        if(eventOptional.isPresent()){
            Event event = eventOptional.get();
//            mailingService.sendEmail(client,"YeeeY!" );
//            Logger.getLogger(getClass()).info(smsService.sendSms());

            return "redirect:/adminEvent/list";
        }

        return "redirect:/adminEvent/list";
    }
    @GetMapping("/remove/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        Optional<Event> optionalEvent = eventService.deleteEventById(id);
        if (optionalEvent.isPresent()) {

            return "redirect:/adminEvent/list";
        }
        return "redirect:/adminEvent/list";


    }
}
