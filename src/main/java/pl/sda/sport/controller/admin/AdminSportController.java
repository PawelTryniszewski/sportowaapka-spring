package pl.sda.sport.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.sport.model.Sport;
import pl.sda.sport.model.dto.SportCreateDto;
import pl.sda.sport.service.ClientService;
import pl.sda.sport.service.SportService;

import java.util.Optional;

@Controller
@RequestMapping("/admin/")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminSportController {

    @Autowired
    private ClientService clientService;
    @Autowired
    private SportService sportService;

//    @Autowired
//    private MailingService mailingService;
//
//    @Autowired
//    private SmsService smsService;

    @GetMapping("/list")
    public String sportList(Model model) {

        model.addAttribute("sportList", sportService.getAll());
        return "admin/client/sport_list";
    }

    @GetMapping("/add")
    public String getSportForm(Model model) {
        model.addAttribute("sport", new SportCreateDto());

        return "admin/client/sport_add";
    }

    @PostMapping("/add")
    public String sendEmployeeForm(SportCreateDto dto, Model model) {
        Optional<Sport> sportOptional = sportService.addSport(dto);
        if(sportOptional.isPresent()){
           Sport sport = sportOptional.get();
//            mailingService.sendEmail(client,"YeeeY!" );
//            Logger.getLogger(getClass()).info(smsService.sendSms());

            return "redirect:/admin/list";
        }

        return "redirect:/admin/list";
    }

    @GetMapping("/remove/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        Optional<Sport> sportOptional = sportService.deleteSportById(id);
        if (sportOptional.isPresent()) {

            return "redirect:/admin/list";
        }
        return "redirect:/admin/list";


    }
}
