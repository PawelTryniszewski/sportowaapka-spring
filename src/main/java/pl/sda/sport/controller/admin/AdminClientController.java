package pl.sda.sport.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.dto.ClientModifyDto;
import pl.sda.sport.service.ClientService;

import java.util.Optional;

@Controller
@RequestMapping("/admin/client/")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminClientController {

    @Autowired
    private ClientService clientService;

//    @Autowired
//    private MailingService mailingService;
//
//    @Autowired
//    private SmsService smsService;
    @GetMapping("/list")
    public String employeeList(Model model) {

        model.addAttribute("clientList", clientService.getAll());
        return "admin/client/list";
    }

    @GetMapping("/add")
    public String getEmployeeForm(Model model) {
        model.addAttribute("client", new ClientModifyDto());

        return "admin/client/clientForm";
    }

    @PostMapping("/add")
    public String sendEmployeeForm(ClientModifyDto dto, Model model) {
        Optional<Client> clientOptional = clientService.register(dto);
        if(clientOptional.isPresent()){
            Client client = clientOptional.get();
//            mailingService.sendEmail(client,"YeeeY!" );
//            Logger.getLogger(getClass()).info(smsService.sendSms());

            return "redirect:/admin/client/list";
        }

        return "redirect:/admin/client/list";
    }

    @GetMapping("/user/{id}")
    public String getEmployeeProfile(Model model) {

        return "admin/client/profile";
    }
    @GetMapping("/remove/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        Optional<Client> optionalClient = clientService.deleteClientById(id);
        if (optionalClient.isPresent()) {

            return "redirect:/admin/client/list";
        }
        return "redirect:/admin/client/list";


    }

}
