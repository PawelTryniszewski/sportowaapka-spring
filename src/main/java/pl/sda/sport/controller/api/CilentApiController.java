package pl.sda.sport.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.dto.ClientModifyDto;
import pl.sda.sport.service.ClientService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("api/clients")
public class CilentApiController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/")
    public ResponseEntity<List<ClientModifyDto>> getClients() {
        return ResponseEntity.ok(clientService.getAll()
                .stream()
                .map(client -> new ClientModifyDto(
                        client.getUsername(),
                        client.getPassword(),
                        null,
                        client.getEmail()))
                .collect(Collectors.toList()));
    }
    @PostMapping("/Add")
    public ResponseEntity<Client> addClient(@RequestParam ClientModifyDto dto){
        clientService.register(dto);
        return ResponseEntity.ok().build();
    }
}
