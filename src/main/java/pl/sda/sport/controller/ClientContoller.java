package pl.sda.sport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.sport.model.Client;
import pl.sda.sport.model.Event;
import pl.sda.sport.model.dto.ClientModifyDto;
import pl.sda.sport.repository.ClientProfileServiceRepository;
import pl.sda.sport.service.*;

import java.time.LocalDate;
import java.util.Optional;

@Controller
@RequestMapping("/client/")
public class ClientContoller {
    @Autowired
    private ClientService clientService;
    @Autowired
    private EventService eventService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private ClientProfileServiceRepository clientProfileServiceRepository;

    @Autowired
    private MailingService mailingService;

    @Autowired
    private SmsService smsService;



    @GetMapping("/add")
    public String getEmployeeForm(Model model) {
        model.addAttribute("client", new ClientModifyDto());

        return "client/clientForm";
    }

    @PostMapping("/add")
    public String sendEmployeeForm(ClientModifyDto dto, Model model) {
        Optional<Client> clientOptional = clientService.register(dto);
        if (clientOptional.isPresent()) {
            Client client = clientOptional.get();
//            mailingService.sendEmail(client, "YeeeY!");
//            Logger.getLogger(getClass()).info(smsService.sendSms());

            return "redirect:/login";
        }

        return "redirect:/";
    }

    @GetMapping("/profile")
    public String employeeList(Model model) {
        Optional<Client> client = loginService.getLoggedInUser();
        if (client.isPresent()) {
            Client foundClient = client.get();
            model.addAttribute("client", foundClient);
            model.addAttribute("events",eventService.getUserEvents(LocalDate.now()));
            model.addAttribute("history",eventService.getUserEventsHistory(LocalDate.now()));
            return "client/profile";
        }
        return "redirect:/";
    }

    @GetMapping("/join/{id}")
    public String joinMe(@PathVariable(name = "id") Long id) {
        Optional<Event> optionalEvent = eventService.joinToEvent(id);
        if (optionalEvent.isPresent()) {

            return "redirect:/client/profile";
        }
        return "redirect:/client/profile";

    }
    @GetMapping("/removeMeFromList/{id}")
    public String removeMe(@PathVariable(name = "id") Long id) {
        Optional<Event> optionalEvent = eventService.removeMeFromList(id);
        if (optionalEvent.isPresent()) {

            return "redirect:/client/profile";
        }
        return "redirect:/client/profile";

    }
    @GetMapping("/listPlayers/{id}")
    public String eventList(@PathVariable(name = "id") Long id, Model model) {

        model.addAttribute("playerList", eventService.getAllPlayers(id));
        return "client/player_list";
    }


}
